# README #

This repository basically contains the node.js server files. 

### What is this repository for? ###

* should help to set up your server
* V 0.1

### How do I get set up? ###

* Install 

nodejs, ruby-full, necessary packets for node with npm, packets for ruby with rubygems, especially rhc (ruby is only needed for the client for the cloudnodejsnode)

* Configuration

Only change the login credentials for mysql in the file provided and you're good to go

* Dependencies

check the file! help for the rhc can be optained from https://www.openshift.com/ (login credentials will be added soon )

* Database configuration


```
#!SQL

-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 15, 2014 at 03:02 AM
-- Server version: 5.5.38-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `nxtgentxt`
--

-- --------------------------------------------------------

--
-- Table structure for table `lastSeen`
--

CREATE TABLE IF NOT EXISTS `lastSeen` (
  `number` varchar(20) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ip` varchar(16) NOT NULL,
  `port` int(11) NOT NULL,
  PRIMARY KEY (`number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lastSeen`
--

INSERT INTO `lastSeen` (`number`, `timestamp`, `ip`, `port`) VALUES
('\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '2014-08-14 23:55:34', 'undefined', 33359),
('232322222', '2014-08-14 23:46:30', '233.152.77.32', 13456);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
```

* Deployment instructions
None yet since only locally tested. I recommend you to do the very same!

### Who do I talk to? ###

* Wer wohl