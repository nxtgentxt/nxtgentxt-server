var config = require('./config.js');
var dgram = require('dgram');
//var storage = require('node-persist');
var socket = dgram.createSocket('udp4');
var mysql = require('mysql');

var MSG_ID_SAYHI = 3
var MSG_ID_ADRESSREQUEST = 4
var MSG_ID_SAYHIACK = 5

socket
    .on('listening', function()
    {
        var address = socket.address();
        console.log('socket listening ' +
            address.address + ':' + address.port);
    })
    .on('error', function(err)
    {
        console.log('socket error:\n' + err.stack);
        socket.close();
    })
    .on('message', function(message, rinfo) {	
	    console.log(message);
	    console.log(rinfo);
	    //storage.initSync();
	    
	    var fragmentid = message.readInt16BE(0);
	    var fragmentnum = message.readInt16BE(2);
	    var msgid = message.readInt32BE(4);
	    var msgtype = msgid/(256*256*256);
	    var msgid = msgid%(256*256*256);
	    
	    var connection =  mysql.createConnection({
        	host : config.mysql.host,
        	user : config.mysql.user,
        	password: config.mysql.password
      });
      connection.connect();
	    connection.query("use nxtgentxt");
      if (msgtype == 3) { //say hi -> data == telephonenumber as string, just wanna say hi + get my ip and port.
        var number = message.toString('utf8',8,24);
        console.log("number = " + number);
        //save number in db along with port and ip!

        var insertQuery = "REPLACE INTO `nxtgentxt`.`lastSeen` (`number`, `timestamp`, `ip`, `port`) VALUES ('"+number+"', CURRENT_TIMESTAMP, '"+rinfo.address+"', '"+rinfo.port+"');";
        connection.query( insertQuery, function(err, rows){
        	if(err)	{
        		throw err;
        	}else{
        		console.log( rows );
        	}
        });
        connection.end(function(err){});
        //send back ip and port
        msgBack = new Buffer(rinfo.address + ':' + rinfo.port);
        fragmentid = 0;
        fragmentnum = 1;
        msgtype = 3;
        var a = new Buffer([0,fragmentid,0,fragmentnum,msgtype,0,0,msgid]);
        var msg = Buffer.concat([a,new Buffer(rinfo.address + ':' + rinfo.port)]);
        socket
            .send(msg, 0, msg.length,
                rinfo.port, rinfo.address,
                function(err, bytes)
                {
                    //socket.close();
                });

      } else if (msgtype == 4) {

        var numbera = message.toString('utf8',8,24);
        var numberb = message.toString('utf8',24,40);
        console.log("number " + numbera + " is requesting " + numberb);
        
        var selectQuery = "SELECT * FROM `nxtgentxt`.`lastSeen` WHERE `number` LIKE '"+numberb+"';";
        console.log(selectQuery);
        connection.query( selectQuery, function(err, rows,fields){
        	if(err)	{
        		throw err;
        	}else{
            var msg = new Buffer(30);
            for (var i in rows) {
              console.log('Post Titles: ', rows[i].ip);
            }
            console.log('huh');
            msg.writeUInt8(1,3);
            msg.writeUInt8(5,4);
            msg.writeUInt8(msgid,7);
            //[0,0,0,1,5,0,0,msgid]);
            if (rows.length == 0) {
              console.log("cant find number " + numberb);
              msg.write(numberb,8,24);
              msg.writeUInt8(0);
              msg.writeUInt8(0);
              msg.writeUInt8(0);
              msg.writeUInt8(0);
              msg.writeUInt16BE(0);
            } else {
              msg.write(numberb,8,24);
              ips = rows[0].ip.split(".");
              for (i = 0; i < ips.length; i++){
                  msg.writeUInt8(parseInt(ips[i]),24+i);
              }
              msg.writeUInt16BE(parseInt(rows[0].port),28);
            }
            //var msg = Buffer.concat([a,new Buffer(numbers[1] + ',' + rows[0].ip + ':' + rows[0].port)]);
            
            
            
            socket
                .send(msg, 0, msg.length,
                    rinfo.port, rinfo.address,
                    function(err, bytes)
                    {
                        //socket.close();
                    });
        		console.log( rows );
        	}
        console.log('sent: ' + msg.toString('utf8',8))
        });

        connection.end(function(err){});

        
      }
    })
    .bind(config.udp.port,"0.0.0.0");
