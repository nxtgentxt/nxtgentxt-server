var config = require('./config.js');
var net = require('net');//TCP Library
var mysql = require('mysql');

// Keep track of the open sockets for the clients
var clients = [];

// Start the TCP Server
net.createServer(function (socket) {

    // Put this new client in the list AND add client to mysql-DB
    clients.push(socket);
    console.log("NEW CONNECTION "+ socket.remoteAddress + ":" + socket.remotePort);
    
    //keep the socket alive and set timout
    socket.setKeepAlive(true);
    socket.setTimeout(15000); // 15 sekonds
    
    //db-connection connect
    var connection =  mysql.createConnection({
    	host : config.mysql.host,
    	user : config.mysql.user,
    	password: config.mysql.password
	});
	connection.connect();
	connection.query("use nxtgentxt");

    // Handle incoming messages from clients.
    socket.on('data', function (data) {

    	//there might be many jsons in one data, split them by \n
    	var lines = (""+data).split(';');
    	lines.forEach(function(data) {
    		if(data != ""){
		        
			    var json = JSON.parse(data);
			    var sender = json.sender;
			    var type = json.type;
			    var token = json.token;
			    
			    if(type > 0){//if no ping then print
			        console.log("MESSAGE = "+data);
			    }
		
			    //verify the security token with the stored private-key of this sender
			    if(true){ //if verified
	                switch (type){

	                    //ping from client
	                    case 0:
	                    	//return pong to client
	                        send(data+'\n', socket, null);
	                        sendPendingMessages(socket);//is it the right way to do with every PING??
	                        break;
	
			            //registrating after offtime (resp. first time!)
			            case 1:
			            	deleteDoublicateSockets(sender, clients, function(){socket.name = sender;});
				            console.log("socket.name = "+socket.name);
				            handleClientRegistering(sender);
				            //confirm registration message (by returning message?)
	            	        send(data+'\n', socket, null);
				            break;
	
			            //regular message with text or image data for me
	                    case 2:
	                    	preventDoublicatedMsg(sender, json.localMsgId, function(){
					            var receiver = json.receiver;
					            for (var i=0; i< receiver.length; i++){
						            findReceiverAndSend(receiver[i], clients, data);
					                storeMessage(receiver, json.localMsgId, data, sender);
				                }
	                    	});
	                    	confirmMsgReception(socket, json.localMsgId);
	                        break;
	
	                    //confirmation for message from receiver
	                    case 3:
	                    	//delete stored type 2 message from db, because successfully delivered
	                    	//herefore take the old original message to transmit and update it. the sender is now the receiver!
	                    	updateStoredMessage(json.receiver[0], json.localMsgId, data, sender);
			                findReceiverAndSend(json.receiver[0], clients, data);//forward confirmation msg
			                console.log("TYPE 3: update stored message");
	                        break;
	
			            //confimation of reception from client for confirmation from receiver :P delete stored confirmation msg of type 3
			            case 4:
			            	console.log("TYPE 4: delete receiver="+json.receiver[0]+" and localId="+json.localMsgId);
	                        deleteStoredMessage(sender, json.receiver[0], json.localMsgId);
				            break;
	
	                    default:
	                        //anything?
	                        break;
                    }
		        }
    		}
    	});
    });

    // Remove the client from the list when it leaves
    socket.on('end', function () {
        clients.splice(clients.indexOf(socket), 1);
        console.log("socket.name = "+socket.name+" has ENDED");
    });

    socket.on('close', function () {
        clients.splice(clients.indexOf(socket), 1);
        console.log("socket.name = "+socket.name+" has CLOSED");
    });

    socket.on('error', function (error) {
        process.stdout.write('Error:' +error.toString());
        clients.splice(clients.indexOf(socket), 1);
    });
    
    socket.on('timeout', function (error) {
    	console.log("socket.name = "+socket.name+" has TIMEDOUT");
        clients.splice(clients.indexOf(socket), 1);
        socket.destroy();
    });

    // Send a message to the receiver (one clients)
    function send(message, receiver, callback) {
    	receiver.write(message, function(err){
       	    if(err) { 
                process.stdout.write('Error', err);
            } else if(typeof(callback) == "function") {
                callback.call();
            }
    	});
    }
    
    function findReceiverAndSend(receiver, clients, data){
        //find receiver in clients[] and send
        var index;
        if((index = searchClients(receiver, clients)) != -1 && !(typeof clients[index] === 'undefined')){
        	console.log("receiver (name = "+clients[index].name+") found (index = "+index+"), sending message.");
            send(data+'\n', clients[index], null);
        }
    }
    
    function confirmMsgReception(senderSocket, localMsgId){
		//notify client that message has been stored in server
		var confirmationMsg = {
            sender: 'SERVER',
            token: '',
            receiver: senderSocket.name,
            localMsgId: localMsgId,
		    type: '4'
		};
        send(JSON.stringify(confirmationMsg)+'\n', socket, null);
    }

    function searchClients(name, array){
	    for (var i = 0; i < array.length; i++) { 
	        if (array[i].name === name) { 
	        	return i;
	        } else if(array.length-1 == i){
                return -1;
            }
    	}
    }
    
    function deleteDoublicateSockets(name, array, callback){
    	//search for the old open socket with same name and kill it, then open a new 
    	var index = 0;
	    while (index < array.length) { 
	        if (array[index].name === name) { 
	        	array.splice(index, 1);
	        } else {
	        	index++;
	        }
	        //if loop finished and clients clean of doublicates
	        if(array.length == index){
	        	callback.call();
            }
    	}
    }
    
    function registerClient(clientName){
    	var insert = "INSERT into client (number) VALUES ('"+clientName+"')";
        connection.query( insert, function(err, rows){
        	if(err)	{
        		console.log("Error inserting : %s ",err );
        	} else {
        		console.log( "inserted client = "+clientName+" into db!" );
        		return true;
        	}
        });
    }
    
    function handleClientRegistering(clientName){
    	var select = "SELECT * from client WHERE number = '"+clientName+"'";
    	connection.query( select, function(err, client) {
        	if(err)	{
                console.log("Error selecting : %s ",err );
        		//throw err;
        	} else {
        		//if client exists in db
        		if(client.length == 1){
        			console.log("client = "+clientName+" is registered");
	            	//searching for pending messages in DB
	            	//sendPendingMessages(socket);//dont do it here, do it with the first pong! otherwise client might get it 2 times!
        		} else {
        			console.log("client = "+clientName+" is NOT yet registered, register");
        			registerClient(clientName);
        		}
        	}
        });
    }
    
    function preventDoublicatedMsg(sender, localId, callback){
    	var select = "SELECT * from message WHERE sender = '"+sender+"' AND localId = '"+localId+"'";
    	connection.query( select, function(err, rows) {
    		if(rows.length == 0) callback.call();
    	});
    }
    
    function sendPendingMessages(socket){
    	var select = "SELECT * from message WHERE receiver = '"+socket.name+"'";
    	connection.query( select, function(err, messages) {
        	if(err)	{
        		console.log("Error selecting : %s ",err );
        	} else {
        		for(var i in messages){
        			send(messages[i].message+'\n', socket, function() {
            			console.log( "pending message has been sent to receiver = "+socket.name );
                    });
        		}
        	}
        });
    }
    
    function updateStoredMessage(receiver, localId, data, sender){
    	//very important: current type has to be 2 (and is updated to type 3), 
    	//otherwise conflict could happen with msg in reverse direction with a type 3 and type 2 msg! sender and receiver has to be switched!!
        var update = "UPDATE message SET receiver = ?, message = ?, sender = ?, type = 3  WHERE localId = ? AND sender = ? AND receiver = ? AND type = '2'";
        connection.query(update, [receiver, data, sender, localId, receiver, sender], function(err, rows) {
             if(err) console.log("Error updating : %s ",err );
        });
    }

    function deleteStoredMessage(sender, receiver, localId){
    	//only type 3 messages can be deleted! and sender and receiver switched again, its fine!
        var del = "DELETE from message WHERE localId = '"+localId+"' AND receiver = '"+sender+"' AND sender = '"+receiver+"'  AND type = '3'";
        connection.query(del, function(err, rows) {
             if(err) console.log("Error deleting : %s ",err );
        });
    }
    
    function storeMessage(receiver, localId, data, sender){
    	var insert = "INSERT into message (receiver, localId, message, sender, type) VALUES ('"+receiver+"', '"+localId+"','"+data+"', '"+sender+"','2')";
    	connection.query( insert, function(err, rows) {
        	if(err)	{
        		console.log("Error inserting : %s ",err );
        	} else {
    			console.log( "store message for receiver = "+receiver );
        	}
        });
    }
}).listen(config.tcp.port, function (){
    console.log('nodeServer listening port: ' + config.tcp.port);
});


